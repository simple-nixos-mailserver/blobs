The Clamav database is updated in place each day. We then need to
store it somewhere, in order to have reproducible tests.

Note this is not used when you use the nixos-mailserver module: this
is only used to run the clamav test.
