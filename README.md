This repository is used to store huge files used by the
nixos-mailserver project.

The goal is to keep the nixos-mailserver repository size small and
download required artifacts at build time.

